#!/usr/bin/env bash


# stop when there's an error
set -e


# this () { echo $(readlink -f $(dirname ${BASH_SOURCE[0]})); }
shopt -s expand_aliases
alias this="readlink -f \$(dirname \${BASH_SOURCE[0]})"



#-------------------------------------------------------------------------------
# CCTBX installer
#

pushd $(this)/..
source activate.sh

python bootstrap.py --builder=dials \
                    --use-conda $CONDA_PREFIX \
                    --nproc=$(nproc) \
                    --config-flags="--enable_cxx11" \
                    --config-flags="--no_bin_python" \
                    hot update build
popd

#-------------------------------------------------------------------------------
