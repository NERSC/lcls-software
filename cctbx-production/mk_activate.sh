#!/usr/bin/env bash


# stop when there's an error
set -e


# this () { echo $(readlink -f $(dirname ${BASH_SOURCE[0]})); }
shopt -s expand_aliases
alias this="readlink -f \$(dirname \${BASH_SOURCE[0]})"


#-------------------------------------------------------------------------------
# ENV Script
#

cat > $(this)/activate.sh <<EOF

source $(this)/conda.local/env.sh
source activate psana_env
if [[ -e $(this)/build/setpaths.sh ]]; then
    source $(this)/build/setpaths.sh
fi

EOF


#-------------------------------------------------------------------------------
