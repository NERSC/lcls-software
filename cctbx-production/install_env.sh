#!/usr/bin/env bash


# stop when there's an error
set -e


# this () { echo $(readlink -f $(dirname ${BASH_SOURCE[0]})); }
shopt -s expand_aliases
alias this="readlink -f \$(dirname \${BASH_SOURCE[0]})"



#-------------------------------------------------------------------------------
# CONDA Env for psana
#


source $(this)/conda.local/env.sh
# conda env create -f $(this)/psana_environment.yml
conda install mamba -c conda-forge
mamba env create -f psana_environment.yml -p $(this)/conda.local/miniconda3
python /img/conda.local/util/patch-rpath.py /img/conda.local/miniconda3/lib

#-------------------------------------------------------------------------------
